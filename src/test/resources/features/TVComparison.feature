Feature: TV comparison tests

  Scenario Outline: Check TVs can be filtered correctly
    Given I am on the TV comparison website
    When I filter on the below fields
      | ScreenSize   | ScreenType   | WhichRecommendation   |
      | <ScreenSize> | <ScreenType> | <WhichRecommendation> |
    Then the TV that matches the criteria is only displayed

    Examples:
      | ScreenSize | ScreenType | WhichRecommendation |
      | 27         | LCD        |                     |
      | 55         | OLED       |                     |

  Scenario: TVs can be added to compare list
    Given I am on the TV comparison website
    When I add the following TVs to the compare list
      | Brand   | Model                    |
      | Philips | 55OLED754/12             |
      | Samsung | The Serif 2019 QE43LS01R |
    Then the above TVs are added to the Compare List

  @ignore
  Scenario Outline: TV list can be sorted
    Given I am on the TV comparison website
    When I sort by <SortOrder>
    Then the list is sorted in that order
    Examples:
      | SortOrder           |
      | Price (low to high) |
