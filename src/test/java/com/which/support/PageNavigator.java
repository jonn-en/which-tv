package com.which.support;

import com.which.pageobjects.TVPage;
import org.openqa.selenium.WebDriver;

public class PageNavigator {

    WebDriver driver;
    String url;

    public PageNavigator(final WebDriver driver, final String url) {
        this.driver = driver;
        this.url = url;
    }

    public TVPage navigateToTVPage() {
        this.driver.navigate().to(this.url + "/reviews/televisions");
        return new TVPage(this.driver);
    }


}
