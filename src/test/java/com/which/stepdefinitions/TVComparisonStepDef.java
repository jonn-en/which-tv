package com.which.stepdefinitions;

import com.which.domain.TVProductInfo;
import com.which.pageobjects.TVPage;
import com.which.support.PageNavigator;
import com.which.support.TestConfig;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.After;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@ContextConfiguration(classes = {TestConfig.class})
public class TVComparisonStepDef {

    private static final Logger LOGGER = LogManager.getLogger(TVComparisonStepDef.class);

    @Autowired
    PageNavigator pageNavigator;

    @Autowired
    WebDriver driver;

    private TVPage tvPage;

    Map<String, String> filters;
    List<Map<String, String>> tvsToAddToCompare;

    @Given("I am on the TV comparison website")
    public void iAmOnTheTVComparisonWebsite() {
        LOGGER.info("Opening the TV Comparison page");
        tvPage = pageNavigator.navigateToTVPage();
    }


    @When("I filter on the below fields")
    public void iFilterOnTheBelowFields(DataTable table) {
        List<Map<String, String>> filterCriteria = table.asMaps(String.class, String.class);
        filters = filterCriteria.get(0);
        String screenSize = filters.get("ScreenSize");
        String screenType = filters.get("ScreenType");
        String whichRecommendation = filters.get("WhichRecommendation");

        if (screenSize != null && !screenSize.isEmpty()) {
            tvPage = tvPage.filterOnScreenSize(screenSize);
        }

        if (screenType != null && !screenType.isEmpty()) {
            //unimplemented
            //tvPage = tvPage.filterOnScreenType(screenType);
        }

        if (whichRecommendation != null && !whichRecommendation.isEmpty()) {
            //unimplemented
            //tvPage = tvPage.filterOnWhichRecommendation(whichRecommendation);
        }
    }

    @Then("the TV that matches the criteria is only displayed")
    public void theTVWhichMatchTheCriteriaIsOnlyDisplayed() {
        List<TVProductInfo> tvProductInfo = tvPage.getAllTVProductInfo();
        List<TVProductInfo> filteredTVs = tvProductInfo.stream().filter(tvProductInfo1 -> {
                    return (tvPage.getSelectedFilterRange()[0] <= tvProductInfo1.getImportantFeatures().getScreeSize()
                            && tvPage.getSelectedFilterRange()[1] >= tvProductInfo1.getImportantFeatures().getScreeSize());
                }
        ).collect(Collectors.toList());
        assertThat(tvProductInfo.size()).as("Right number of TV are filtered").isEqualTo(filteredTVs.size());
    }

    @When("I add the following TVs to the compare list")
    public void iAddTheFollowingTVsToTheCompareList(DataTable table) {
        tvsToAddToCompare = table.asMaps(String.class, String.class);
        tvsToAddToCompare.forEach(tvToAddToCompare -> {
            tvPage = tvPage.addToCompare(tvToAddToCompare.get("Brand"), tvToAddToCompare.get("Model"));
        });
    }

    @Then("the above TVs are added to the Compare List")
    public void theAboveTVsAreAddedToTheCompareList() {
        List<Map<String, String>> tVsInCompareList = tvPage.getTVsInCompareList();
        assertThat(tVsInCompareList).usingRecursiveComparison().isEqualTo(tvsToAddToCompare);
    }

    @After
    public void tearDown(Scenario scenario) {
        if (scenario.isFailed()) {
        final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
        scenario.embed(screenshot, "image/png", "");
        }
    }
}
