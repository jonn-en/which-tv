package com.which.domain;

public class ImportantTVFeatures {
    private int screeSize;
    private String screenType;
    private String hdStatus;

    public int getScreeSize() {
        return screeSize;
    }

    public ImportantTVFeatures setScreeSize(int screeSize) {
        this.screeSize = screeSize;
        return this;
    }

    public String getScreenType() {
        return screenType;
    }

    public ImportantTVFeatures setScreenType(String screenType) {
        this.screenType = screenType;
        return this;
    }

    public String getHdStatus() {
        return hdStatus;
    }

    public ImportantTVFeatures setHdStatus(String hdStatus) {
        this.hdStatus = hdStatus;
        return this;
    }

    public String toString() {
        return "screeSize: " + screeSize + "\nscreenType: " + screenType + "\nhdStatus: " + hdStatus;
    }
}
