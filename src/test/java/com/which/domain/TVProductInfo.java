package com.which.domain;

import org.openqa.selenium.WebElement;

import java.math.BigDecimal;

public class TVProductInfo {

    private String brandName;

    private String modelName;

    private ImportantTVFeatures importantFeatures;

    private BigDecimal price;

    private WebElement compareElement;

    public String getBrandName() {
        return brandName;
    }

    public TVProductInfo setBrandName(String brandName) {
        this.brandName = brandName;
        return this;
    }

    public String getModelName() {
        return modelName;
    }

    public TVProductInfo setModelName(String modelName) {
        this.modelName = modelName;
        return this;
    }

    public ImportantTVFeatures getImportantFeatures() {
        return importantFeatures;
    }

    public TVProductInfo setImportantFeatures(ImportantTVFeatures importantFeatures) {
        this.importantFeatures = importantFeatures;
        return this;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public TVProductInfo setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public WebElement getCompareElement() {
        return compareElement;
    }

    public TVProductInfo setCompareElement(WebElement compareElement) {
        this.compareElement = compareElement;
        return this;
    }

    public String toString() {
        return "brandName: " + brandName + "\nmodelName: " + modelName + "\nimportantFeatures: " + importantFeatures.toString() + "\nprice: " + price + "\ncompareElementTag: " + compareElement.getTagName();

    }
}
