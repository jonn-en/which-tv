package com.which.pageobjects;

import com.which.domain.ImportantTVFeatures;
import com.which.domain.TVProductInfo;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class TVPage extends Page {

    public TVPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "[data-which-button='which_recommendations-filter']")
    private WebElement whichRecommendsFilter;

    @FindBy(css = "[data-which-button='screen_size-filter']")
    private WebElement screenSizeFilter;

    @FindBy(css = "[data-which-button='screen_type-filter']")
    private WebElement screenTypeFilter;

    @FindBy(css = "[data-which-button='more_filters-filter']")
    private WebElement moreFilter;

    @FindBy(css = "data-which-id='comparison-flyout-tray'")
    private WebElement comparisonBlock;

    private int[] selectedFilterRange;

    List<TVProductInfo> tvProductInfoList = new ArrayList<>();

    public int[] getSelectedFilterRange() {
        return selectedFilterRange;
    }

    /**
     * Builds and sets up TV information for all the visible TVs on the page
     */
    private void setTVProductInfo() {
        List<WebElement> productCardElements = driver.findElements(By.cssSelector("[data-which-id='product-card']"));
        for (WebElement productCardElement : productCardElements) {
            TVProductInfo tvProductInfo = new TVProductInfo();
            List<String> impFeatures = productCardElement.findElements(By.cssSelector("[data-which-id='important-feature']"))
                    .stream()
                    .map(WebElement::getText)
                    .collect(Collectors.toList());

            tvProductInfo.setBrandName(productCardElement.findElement(By.cssSelector("[itemprop='manufacturer']")).getText())
                    .setModelName(productCardElement.findElement(By.cssSelector("[itemprop='model']")).getText())
                    .setPrice(new BigDecimal(productCardElement.findElement(By.cssSelector("[data-test-element='product-amount']"))
                            .getText()
                            .replaceAll("[^\\d]", "")
                            .trim()))
                    .setImportantFeatures(new ImportantTVFeatures().setScreeSize(Integer.parseInt(impFeatures.get(0).replaceAll("[^\\d]", "").trim()))
                            .setScreenType(impFeatures.get(1))
                            .setHdStatus(impFeatures.get(2)))
                    .setCompareElement(productCardElement.findElement(By.cssSelector("[data-which-id$='compare-button']")));
            tvProductInfoList.add(tvProductInfo);
        }
    }

    /**
     * Returns TV information  for the passed brand and model name
     * @param bandName - Brand name of the TV
     * @param modelName - model name of the TV
     * @return - A {@link TVProductInfo} object
     */
    public TVProductInfo findTVProductInfo(String bandName, String modelName) {
        if (tvProductInfoList.size() == 0) {
            setTVProductInfo();
        }
        return tvProductInfoList.stream().filter(tvProductInfo -> tvProductInfo.getBrandName().trim().equalsIgnoreCase(bandName) && tvProductInfo.getModelName().trim().equalsIgnoreCase(modelName))
                .findFirst()
                .orElse(null);
    }

    /**
     * Adds a TV to the compare block
     * @param bandName - Brand name of the TV
     * @param modelName - model name of the TV
     * @return - an instance of the page
     */
    public TVPage addToCompare(String bandName, String modelName) {
        WebElement compareElement = findTVProductInfo(bandName, modelName).getCompareElement();
        js.executeScript("arguments[0].scrollIntoView();", compareElement.findElement(By.xpath("ancestor::li")));
        compareElement.click();
        return this;
    }

    /**
     * Filters the page by selecting a screen size range in the dropdown
     * @param screenSize - screen size that falls in a pre-defined range
     * @return an instance of the page
     */
    public TVPage filterOnScreenSize(String screenSize) {
        screenSizeFilter.click();
        List<WebElement> screenSizesEl = driver.findElements(By.cssSelector("[data-test-element='facet-menu'] label[for^='screen_size']"));
        for (WebElement screenSizeEl : screenSizesEl) {
            int[] screenRange = Arrays.stream(screenSizeEl.findElement(By.cssSelector("input")).getAttribute("value").split("-"))
                    .mapToInt(Integer::parseInt)
                    .toArray();
            if (screenRange[0] <= Integer.parseInt(screenSize) && Integer.parseInt(screenSize) <= screenRange[1]) {
                screenSizeEl.findElement(By.cssSelector("span[data-test-element='checkbox-filter__inner-label']")).click();
                selectedFilterRange = screenRange;
            }
        }
        WebElement doneButton = driver.findElement(By.cssSelector("[data-which-id='screen_size-filter-summary'] [data-which-id='done-button']"));
        WebDriverWait wait = new WebDriverWait(driver, 2, 20);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("[data-test-element='loading-indicator']")));
        doneButton.click();
        return this;
    }

    /**
     * Get details of TVs in the compare list
     * @return
     */
    public List<Map<String, String>> getTVsInCompareList() {
        List<Map<String, String>> tvDetailsInCompareList = new ArrayList<>();
        driver.findElements(By.cssSelector("[data-which-id='comparison-flyout-product-name']")).forEach(tvsInCompareList -> {
            Map<String, String> tvDetails = new HashMap<>();
            tvDetails.put("Brand", (tvsInCompareList.findElements(By.cssSelector("span"))).get(0).getText());
            tvDetails.put("Model", (tvsInCompareList.findElements(By.cssSelector("span"))).get(1).getText());
            tvDetailsInCompareList.add(tvDetails);
        });
        return tvDetailsInCompareList;
    }

    public List<TVProductInfo> getAllTVProductInfo() {
        setTVProductInfo();
        return tvProductInfoList;
    }
}
