package com.which.pageobjects;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public abstract class Page {

    WebDriver driver;
    JavascriptExecutor js;

    public Page(WebDriver driver){
        this.driver = driver;
        js = (JavascriptExecutor) driver;
    }




}
