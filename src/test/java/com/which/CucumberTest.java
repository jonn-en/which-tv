package com.which;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "classpath:features",
        glue = "com.which.stepdefinitions",
        tags = {"not @ignore"},
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        plugin = {"pretty","usage:target/usage.json", "html:target/cucumber-reports"},
        monochrome = true)
public class CucumberTest {
}
