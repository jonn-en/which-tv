# Introduction
This test framework tests a few functionality on the TV Comparison page. I have implemented two of those tests and left one unimplemented. 

# Features/Design of the framework
This framework is written in Java and uses Selenium WebDriver and Cucumber.

# Requirements
- JDK 1.8
- Maven 3 or higher
- `chromedriver` downloaded for your OS/chrome browser combination and
  placed in `<project_root>/src/test/resources/driver`. It can be
  downloaded from [here](http://chromedriver.chromium.org/downloads).

# Running
To run all the tests, run the below command in the project root
```
mvn test
```


## Left out
- There Reporting is very basic due to lack of time.

# Reports
Reports are generated in the `target/cucumber-reports` folder. The
report is overwritten every time the tests are run.